#pragma once
#include <vulkan/vulkan.hpp>
#include <memory>

class Texture
{
public:
	Texture(const vk::Device& logicalDevice, const vk::PhysicalDevice physicalDevice, vk::SampleCountFlagBits msaaSamples) 
		: m_logicalDevice(logicalDevice), m_physicalDevice(physicalDevice), m_msaaSamples(msaaSamples) {};
	~Texture();
	void CleanUp();

	const vk::Image& Image() const { return m_image; };
	const vk::DeviceMemory& DeviceMem() const { return m_imageMemory; };
	const vk::ImageView& ImageView() const { return m_imageView; };

	void CreateImageView(vk::Format format, vk::ImageAspectFlags aspectFlags, uint32_t mipLevels);

	void CreateImage(uint32_t width, uint32_t height, uint32_t mipLevels, vk::SampleCountFlagBits numSamples, vk::Format format,
		vk::ImageTiling tiling, vk::ImageUsageFlags usage, vk::MemoryPropertyFlags properties);


	void TransitionImageLayout(vk::Format format, vk::CommandBuffer commandBuffer, vk::ImageLayout oldLayout, vk::ImageLayout newLayout, uint32_t mipLevels);
	void GenerateMipmaps(vk::Format imageFormat, vk::CommandBuffer commandBuffer, int32_t texWidth, int32_t texHeight, uint32_t mipLevels);

	void CopyBufferToImage(const vk::Buffer& buffer, vk::CommandBuffer commandBuffer, const uint32_t width, const uint32_t height);


protected:


	//---DUPLICATED FROM MAIN //////////////////////////////////////////////////////////////////////////
	uint32_t FindMemoryType(uint32_t typeFilter, vk::MemoryPropertyFlags properties); //- put into a memory class (or something else?)?
	bool HasStencilComponent(vk::Format format); //-small helper function, prob not needed on main?
	//////////////////////////////////////////////////////////////////////////
private:
	vk::Image m_image;
	vk::DeviceMemory m_imageMemory;
	vk::ImageView m_imageView;

	vk::Device m_logicalDevice;
	vk::PhysicalDevice m_physicalDevice; //only needed for FindMemoryType?

	vk::SampleCountFlagBits m_msaaSamples;

};


