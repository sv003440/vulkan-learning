#include "main.h"
#include <set>
#include <algorithm>

HelloTriangleApp::HelloTriangleApp() 
{

}

HelloTriangleApp::~HelloTriangleApp()
{
	//CleanUp();
}

void HelloTriangleApp::Run()
{
	InitWindow();
	InitVulkan();
	MainLoop();
	CleanUp();
}

void HelloTriangleApp::InitWindow()
{
	glfwInit();

	//tells GLFW NOT to create an OpenGL context, because we aren't using OpenGL
	glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);

	m_window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan", nullptr, nullptr);
	glfwSetWindowUserPointer(m_window, this);
	glfwSetFramebufferSizeCallback(m_window, FramebufferResizeCallBack);
}

void HelloTriangleApp::InitVulkan()
{
	CreateInstance();
	SetupDebugMessenger();
	CreateSurface();
	PickPhysicalDevice();
	CreateLogicalDevice();
	CreateSwapChain();
	CreateSwapChainImageViews();
	CreateRenderPass();
	CreateDescriptorSetLayout();
	CreateGraphicsPipeline();
	CreateCommandPool();
	CreateColourResources();
	CreateDepthResources();
	CreateFramebuffers();
	CreateTextureImage();
	CreateTextureImageView();
	CreateTextureSampler();
	LoadModel();
	CreateVertexBuffer();
	CreateIndexBuffer();
	CreateUniformBuffers();
	CreateDescriptorPool();
	CreateDescriptorSets();
	CreateCommandBuffers();
	CreateSyncObjects();
}

void HelloTriangleApp::PickPhysicalDevice()
{
	//uint32_t deviceCount = 0;
	//vkEnumeratePhysicalDevices(m_instance, &deviceCount, nullptr);

	//if (deviceCount == 0)
	//{
	//	throw std::runtime_error("failed to find any GPUs with Vulkan support");
	//}
	
	std::vector<vk::PhysicalDevice> devices = m_instance.enumeratePhysicalDevices();

	for (const auto& device : devices)
	{
		if (IsPhysicalDeviceSuitable(device))
		{
			m_physicalDevice = device;
			m_msaaSamples = GetMaxUsableSampleCount();
			break;
		}
	}

	if (!m_physicalDevice)
	{
		throw std::runtime_error("failed to find a suitable GPU :(");
	}
}

void HelloTriangleApp::MainLoop()
{
	while (!glfwWindowShouldClose(m_window))
	{
		glfwPollEvents();
		//ProcessInput
		//UpdateFrame
		DrawFrame();
	}
	m_logicalDevice.waitIdle();
}

void HelloTriangleApp::CleanUp()
{
	CleanUpSwapChain();

	m_logicalDevice.destroySampler(m_textureSampler);
	m_texture->CleanUp();

	m_logicalDevice.destroyDescriptorSetLayout(m_descriptorSetLayout);

	m_logicalDevice.destroyBuffer(m_vertexBuffer);
	m_logicalDevice.freeMemory(m_vertexBufferMem);

	m_logicalDevice.destroyBuffer(m_indexBuffer);
	m_logicalDevice.freeMemory(m_indexBufferMem);

	for (auto semaphore : m_imageAvailableSemaphores)
	{
		m_logicalDevice.destroySemaphore(semaphore);
	}
	m_imageAvailableSemaphores.clear();

	for (auto semaphore : m_renderFinishedSemaphores)
	{
		m_logicalDevice.destroySemaphore(semaphore);
	}
	m_renderFinishedSemaphores.clear();

	for (auto fence : m_inFlightFences)
	{
		m_logicalDevice.destroyFence(fence);
	}
	m_inFlightFences.clear();

	m_logicalDevice.destroyCommandPool(m_commandPool);

	m_logicalDevice.destroy();

	if (g_validationLayersEnabled)
	{
		DestroyDebugUtilsMessengerEXT(m_instance, m_debugMessenger, nullptr);
	}

	m_instance.destroySurfaceKHR(m_surface);
	m_instance.destroy();

	glfwDestroyWindow(m_window);

	glfwTerminate();
}

void HelloTriangleApp::CreateInstance()
{
	if (g_validationLayersEnabled && !CheckValidationLayerSupport())
	{
		throw std::runtime_error("validation layers requested were not available");
	}

	auto appInfo = vk::ApplicationInfo()
		.setPApplicationName("Hello Triangle")
		.setApplicationVersion(VK_MAKE_VERSION(1, 0, 0))
		.setPEngineName("No Engine")
		.setApiVersion(VK_API_VERSION_1_0);

	auto extensions = GetRequiredExtensions();

	auto createInfo = vk::InstanceCreateInfo()
		.setPApplicationInfo(&appInfo)
		.setEnabledExtensionCount(static_cast<uint32_t>(extensions.size()))
		.setPpEnabledExtensionNames(extensions.data());

	VkDebugUtilsMessengerCreateInfoEXT debugCreateInfo;
	if (g_validationLayersEnabled)
	{
		createInfo.enabledLayerCount = static_cast<uint32_t>(g_validationLayers.size());
		createInfo.ppEnabledLayerNames = g_validationLayers.data();

		PopulateDebugMessengerCreateInfo(debugCreateInfo);
		createInfo.pNext = (VkDebugUtilsMessengerCreateInfoEXT*)&debugCreateInfo;
	}
	else
	{
		createInfo.enabledLayerCount = 0;
		createInfo.pNext = nullptr;
	}

	m_instance = vk::createInstance(createInfo, nullptr);
}

bool HelloTriangleApp::CheckValidationLayerSupport()
{
	uint32_t layerCount;
	vkEnumerateInstanceLayerProperties(&layerCount, nullptr);

	std::vector<VkLayerProperties> availableLayers(layerCount);
	vkEnumerateInstanceLayerProperties(&layerCount, availableLayers.data());

	for (const auto& layerName : g_validationLayers)
	{
		bool layerFound = false;

		for (const auto& layerProperties : availableLayers)
		{
			if (strcmp(layerName, layerProperties.layerName) == 0)
			{
				layerFound = true;
				break;
			}
		}

		if (!layerFound)
		{
			return false;
		}
	}

	return true;
}

bool HelloTriangleApp::CheckDeviceExtensionSupport(vk::PhysicalDevice device)
{
	uint32_t extensionCount;
	vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, nullptr);

	std::vector<VkExtensionProperties> availableExtensions(extensionCount);
	vkEnumerateDeviceExtensionProperties(device, nullptr, &extensionCount, availableExtensions.data());

	std::set<std::string> requiredExtensions(g_deviceExtensions.begin(), g_deviceExtensions.end());
	
	for (const auto& extension : availableExtensions)
	{
		requiredExtensions.erase(extension.extensionName);
	}

	return requiredExtensions.empty();
}

std::vector<const char*> HelloTriangleApp::GetRequiredExtensions()
{
	uint32_t glfwExtensionCount = 0;
	const char** glfwExtensions;
	glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

	std::vector<const char*> extensions(glfwExtensions, glfwExtensions + glfwExtensionCount);

	if (g_validationLayersEnabled)
	{
		extensions.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
	}

	return extensions;
}

vk::Format HelloTriangleApp::FindSupportedFormat(const std::vector<vk::Format>& candidates, vk::ImageTiling tiling, vk::FormatFeatureFlags features)
{
	for (auto format : candidates)
	{
		auto properties = m_physicalDevice.getFormatProperties(format);

		if (tiling == vk::ImageTiling::eLinear && (properties.linearTilingFeatures & features) == features)
		{
			return format;
		}
		else if (tiling == vk::ImageTiling::eOptimal && (properties.optimalTilingFeatures & features) == features)
		{
			return format;
		}
	}

	throw std::runtime_error("Failed to find supported format");
}

vk::Format HelloTriangleApp::FindDepthFormat()
{
	return FindSupportedFormat({ vk::Format::eD32Sfloat, vk::Format::eD32SfloatS8Uint, vk::Format::eD24UnormS8Uint },
		vk::ImageTiling::eOptimal, vk::FormatFeatureFlagBits::eDepthStencilAttachment);
}

void HelloTriangleApp::SetupDebugMessenger()
{
	if (!g_validationLayersEnabled)
	{
		return;
	}

	VkDebugUtilsMessengerCreateInfoEXT createInfo;
	PopulateDebugMessengerCreateInfo(createInfo);

	if (CreateDebugUtilsMessengerEXT(m_instance, &createInfo, nullptr, &m_debugMessenger) != VK_SUCCESS)
	{
		throw std::runtime_error("failed to setup debug messenger");
	}

	if (m_debugMessenger == nullptr)
	{
		throw::std::runtime_error("debug messenger is null for some reason");
	}
}

void HelloTriangleApp::PopulateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo)
{
	createInfo = {};
	createInfo.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
	createInfo.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
	createInfo.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
	createInfo.pfnUserCallback = debugCallback;
	createInfo.pUserData = nullptr; // Optional

}

bool HelloTriangleApp::IsPhysicalDeviceSuitable(vk::PhysicalDevice device)
{
	QueueFamilyIndices indices = FindQueueFamilies(device);

	bool isExtensionsSupported = CheckDeviceExtensionSupport(device);

	bool isSwapChainOK = false;
	if (isExtensionsSupported)
	{
		auto swapChainSupport = QuerySwapChainSupport(device);
		isSwapChainOK = !swapChainSupport.formats.empty() && !swapChainSupport.presentModes.empty();
	}

	auto supportedFeatures = device.getFeatures();

	return indices.IsComplete() && isExtensionsSupported && isSwapChainOK && supportedFeatures.samplerAnisotropy;
}

QueueFamilyIndices HelloTriangleApp::FindQueueFamilies(vk::PhysicalDevice device)
{
	QueueFamilyIndices indices;
		
	uint32_t queueFamilyCount = 0;
	vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, nullptr);

	std::vector<vk::QueueFamilyProperties> queueFamilies(queueFamilyCount);
	//vkGetPhysicalDeviceQueueFamilyProperties(device, &queueFamilyCount, queueFamilies.data());

	queueFamilies = device.getQueueFamilyProperties();

	int i = 0;
	
	for (const auto& queueFamily : queueFamilies)
	{
		//will get improved performance if the same queue family is used for both below things. Could write code to prefer that situation?
		if (queueFamily.queueCount > 0 && queueFamily.queueFlags & vk::QueueFlagBits::eGraphics)
		{
			indices.graphicsFamily = i;
		}

		VkBool32 hasPresentSupport = false;
		//vkGetPhysicalDeviceSurfaceSupportKHR(device, i, m_surface, &hasPresentSupport);

		device.getSurfaceSupportKHR(i, m_surface, &hasPresentSupport);

		if (queueFamily.queueCount > 0 && hasPresentSupport)
		{
			indices.presentFamily = i;
		}

		if (indices.IsComplete())
		{
			break;
		}

		i++;
	}


	return indices;
}

SwapChainSupportDetails HelloTriangleApp::QuerySwapChainSupport(vk::PhysicalDevice device)
{
	SwapChainSupportDetails details;

	//vkGetPhysicalDeviceSurfaceCapabilitiesKHR(device, m_surface, &details.capabilities);

	/*uint32_t formatCount = 0;
	vkGetPhysicalDeviceSurfaceFormatsKHR(device, m_surface, &formatCount, nullptr);*/

	//if (formatCount != 0)
	//{
	//	details.formats.resize(formatCount);
	//	vkGetPhysicalDeviceSurfaceFormatsKHR(device, m_surface, &formatCount, details.formats.data());
	//}



	//uint32_t presentModeCount = 0;
	//vkGetPhysicalDeviceSurfacePresentModesKHR(device, m_surface, &presentModeCount, nullptr);

	//if (presentModeCount != 0)
	//{
	//	details.presentModes.resize(presentModeCount);
	//	vkGetPhysicalDeviceSurfacePresentModesKHR(device, m_surface, &presentModeCount, details.presentModes.data());
	//}

	details.capabilities = device.getSurfaceCapabilitiesKHR(m_surface);

	details.formats = device.getSurfaceFormatsKHR(m_surface);

	details.presentModes = device.getSurfacePresentModesKHR(m_surface);

	return details;
}

vk::SurfaceFormatKHR HelloTriangleApp::ChooseSwapSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& availableFormats)
{
	for (const auto& availableFormat : availableFormats)
	{
			if (availableFormat.format == vk::Format::eB8G8R8A8Unorm &&
				availableFormat.colorSpace == vk::ColorSpaceKHR::eSrgbNonlinear)
		{
			return availableFormat;
		}
	}

	return availableFormats[0];
}

vk::PresentModeKHR HelloTriangleApp::ChooseSwapPresentMode(const std::vector<vk::PresentModeKHR>& availablePresentModes)
{
	for (const auto& availablePresentMode : availablePresentModes)
	{
		if (availablePresentMode == vk::PresentModeKHR::eMailbox)
		{
			return availablePresentMode;
		}
		
	}

	return vk::PresentModeKHR::eFifo; //guaranteed to be available but it's nice to look for mailbox first
}

vk::Extent2D HelloTriangleApp::ChooseSwapExtent(const vk::SurfaceCapabilitiesKHR& capabilties)
{
	if (capabilties.currentExtent.width != std::numeric_limits<uint32_t>::max())
	{
		return capabilties.currentExtent;
	}
	else
	{
		int width, height;
		glfwGetFramebufferSize(m_window, &width, &height);

		vk::Extent2D actualExtent = { static_cast<uint32_t>(width), static_cast<uint32_t>(height)};

		actualExtent.width = std::max(capabilties.minImageExtent.width, std::min(capabilties.maxImageExtent.width, actualExtent.width));
		actualExtent.height = std::max(capabilties.minImageExtent.height, std::min(capabilties.maxImageExtent.height, actualExtent.height));

		return actualExtent;
	}
}

vk::ShaderModule HelloTriangleApp::CreateShaderModule(const std::vector<char>& code)
{
	auto info = vk::ShaderModuleCreateInfo()
		.setCodeSize(code.size())
		.setPCode(reinterpret_cast<const uint32_t*>(code.data()));

	auto shaderModule = m_logicalDevice.createShaderModule(info);

	return shaderModule;
}

//The typeFilter parameter will be used to specify the bit field of memory types that are suitable,
	//so if that bit is set to 1, that memory type is suitable (for the vertex buffer)
	//the properties param is used to check for other things we may be interested in, eg CPU write access
uint32_t HelloTriangleApp::FindMemoryType(uint32_t typeFilter, vk::MemoryPropertyFlags properties)
{
	auto memProperties = m_physicalDevice.getMemoryProperties();
	
	for (uint32_t i = 0; i < memProperties.memoryTypeCount; ++i)
	{
		if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
		{
			return i; //return the index of the memory type that satisfies everything we are looking for
		}
	}

	throw std::runtime_error("failed to find suitable memory type");
}

vk::SampleCountFlagBits HelloTriangleApp::GetMaxUsableSampleCount()
{
	VkPhysicalDeviceProperties physicalDeviceProperties;
	vkGetPhysicalDeviceProperties(m_physicalDevice, &physicalDeviceProperties);

	VkSampleCountFlags counts = std::min(physicalDeviceProperties.limits.framebufferColorSampleCounts, physicalDeviceProperties.limits.framebufferDepthSampleCounts);
	if (counts & VK_SAMPLE_COUNT_64_BIT) { return static_cast<vk::SampleCountFlagBits>(VK_SAMPLE_COUNT_64_BIT); }
	if (counts & VK_SAMPLE_COUNT_32_BIT) { return static_cast<vk::SampleCountFlagBits>(VK_SAMPLE_COUNT_32_BIT); }
	if (counts & VK_SAMPLE_COUNT_16_BIT) { return static_cast<vk::SampleCountFlagBits>(VK_SAMPLE_COUNT_16_BIT); }
	if (counts & VK_SAMPLE_COUNT_8_BIT) { return static_cast<vk::SampleCountFlagBits>(VK_SAMPLE_COUNT_8_BIT); }
	if (counts & VK_SAMPLE_COUNT_4_BIT) { return static_cast<vk::SampleCountFlagBits>(VK_SAMPLE_COUNT_4_BIT); }
	if (counts & VK_SAMPLE_COUNT_2_BIT) { return static_cast<vk::SampleCountFlagBits>(VK_SAMPLE_COUNT_2_BIT); }

	return static_cast<vk::SampleCountFlagBits>(VK_SAMPLE_COUNT_1_BIT);
}

vk::CommandBuffer HelloTriangleApp::BeginSingleTimeCommands()
{
	auto allocInfo = vk::CommandBufferAllocateInfo()
		.setLevel(vk::CommandBufferLevel::ePrimary)
		.setCommandPool(m_commandPool)
		.setCommandBufferCount(1);

	auto commandBuffer = m_logicalDevice.allocateCommandBuffers(allocInfo);

	auto beginInfo = vk::CommandBufferBeginInfo()
		.setFlags(vk::CommandBufferUsageFlagBits::eOneTimeSubmit);

	commandBuffer[0].begin(beginInfo);

	return commandBuffer[0];
}

void HelloTriangleApp::EndSingleTimeCommands(vk::CommandBuffer commandBuffer)
{
	commandBuffer.end();

	auto submitInfo = vk::SubmitInfo()
		.setCommandBufferCount(1)
		.setPCommandBuffers(&commandBuffer);

	m_graphicsQueue.submit(submitInfo, nullptr);
	m_graphicsQueue.waitIdle();

	m_logicalDevice.freeCommandBuffers(m_commandPool, commandBuffer);
}

void HelloTriangleApp::CreateLogicalDevice()
{
	QueueFamilyIndices indices = FindQueueFamilies(m_physicalDevice);

	std::vector<vk::DeviceQueueCreateInfo> queueCreateInfos;
	std::set<uint32_t> uniqueQueueFamilies = { indices.graphicsFamily.value(), indices.presentFamily.value() };

	float queuePriority = 1.0f;
	for (auto queueFamily : uniqueQueueFamilies)
	{
		auto queueCreateInfo = vk::DeviceQueueCreateInfo()
			.setQueueFamilyIndex(queueFamily)
			.setPQueuePriorities(&queuePriority)
			.setQueueCount(1);

		queueCreateInfos.push_back(queueCreateInfo);
	}

	const auto deviceFeatures = vk::PhysicalDeviceFeatures()
		.setSamplerAnisotropy(VK_TRUE)
		.setSampleRateShading(VK_TRUE);

	auto deviceCreateInfo = vk::DeviceCreateInfo()
		.setPQueueCreateInfos(queueCreateInfos.data())
		.setQueueCreateInfoCount(static_cast<uint32_t>(queueCreateInfos.size()))
		.setPEnabledFeatures(&deviceFeatures)
		.setEnabledExtensionCount(static_cast<uint32_t>(g_deviceExtensions.size()))
		.setPpEnabledExtensionNames(g_deviceExtensions.data());

	if (g_validationLayersEnabled)
	{
		deviceCreateInfo.enabledLayerCount = static_cast<uint32_t>(g_validationLayers.size());
		deviceCreateInfo.ppEnabledLayerNames = g_validationLayers.data();
	}
	else
	{
		deviceCreateInfo.enabledLayerCount = 0;
	}

	//if (vkCreateDevice(m_physicalDevice, &createInfo, nullptr, m_logicalDevice) != VK_SUCCESS)
	//{
	//	throw std::runtime_error("failed to create logical device");
	//}

	m_logicalDevice = m_physicalDevice.createDevice(deviceCreateInfo); //throws runtime error automatically if something goes wrong

	m_logicalDevice.getQueue(indices.graphicsFamily.value(), 0, &m_graphicsQueue);
	m_logicalDevice.getQueue(indices.presentFamily.value(), 0, &m_presentQueue);
	
}

void HelloTriangleApp::CreateSurface()
{
	auto surface = static_cast<VkSurfaceKHR>(m_surface);

	if (glfwCreateWindowSurface(m_instance, m_window, nullptr, &surface) != VK_SUCCESS)
	{
		throw std::runtime_error("failed to create window surface");
	}	
	m_surface = surface; //work around for using vk::...  instead of Vk... stuff
						//hopefully there is a more elegant way but I don't know it

}

void HelloTriangleApp::CreateSwapChain()
{
	auto swapChainSupport = QuerySwapChainSupport(m_physicalDevice);

	auto surfaceFormat = ChooseSwapSurfaceFormat(swapChainSupport.formats);
	auto presentMode = ChooseSwapPresentMode(swapChainSupport.presentModes);
	auto extent = ChooseSwapExtent(swapChainSupport.capabilities);

	uint32_t imageCount = swapChainSupport.capabilities.minImageCount + 1;
	if (swapChainSupport.capabilities.maxImageCount > 0 && imageCount > swapChainSupport.capabilities.maxImageCount)
	{
		imageCount = swapChainSupport.capabilities.maxImageCount;
	}

	auto createInfo = vk::SwapchainCreateInfoKHR()
		.setSurface(m_surface)
		.setMinImageCount(imageCount)
		.setImageFormat(surfaceFormat.format)
		.setImageColorSpace(surfaceFormat.colorSpace)
		.setImageExtent(extent)
		.setImageArrayLayers(1)
		.setImageUsage(vk::ImageUsageFlagBits::eColorAttachment);//this means images will be rendered to swap chain directly, change this for post processing affects

	auto indices = FindQueueFamilies(m_physicalDevice);
	uint32_t queueFamilyIndices[] = { indices.graphicsFamily.value(), indices.presentFamily.value() };

	if (indices.graphicsFamily != indices.presentFamily)
	{
		createInfo.imageSharingMode = vk::SharingMode::eConcurrent;// = VK_SHARING_MODE_CONCURRENT;
		createInfo.queueFamilyIndexCount = 2;
		createInfo.pQueueFamilyIndices = queueFamilyIndices;
	}
	else
	{
		createInfo.imageSharingMode = vk::SharingMode::eExclusive;//= VK_SHARING_MODE_EXCLUSIVE;
		createInfo.queueFamilyIndexCount = 0;
		createInfo.pQueueFamilyIndices = nullptr;
	}
	createInfo.preTransform = swapChainSupport.capabilities.currentTransform;
	createInfo.compositeAlpha = vk::CompositeAlphaFlagBitsKHR::eOpaque;// = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
	createInfo.presentMode = presentMode;
	createInfo.clipped = VK_TRUE;
	createInfo.oldSwapchain = nullptr;

	m_swapChain = m_logicalDevice.createSwapchainKHR(createInfo, nullptr);

	m_swapChainImages = m_logicalDevice.getSwapchainImagesKHR(m_swapChain);

	m_swapChainImageFormat = surfaceFormat.format;
	m_swapChainExtent = extent;
}

vk::ImageView HelloTriangleApp::CreateImageView(vk::Image image, vk::Format format, vk::ImageAspectFlags aspectFlags, uint32_t mipLevels)
{
	auto subResourceRange = vk::ImageSubresourceRange()
		.setAspectMask(aspectFlags)
		.setBaseMipLevel(0)
		.setLevelCount(mipLevels)
		.setBaseArrayLayer(0)
		.setLayerCount(1);

	auto createInfo = vk::ImageViewCreateInfo()
		.setImage(image)
		.setViewType(vk::ImageViewType::e2D)
		.setFormat(format)
		//.setComponents(mapping)
		.setSubresourceRange(subResourceRange);

	return m_logicalDevice.createImageView(createInfo);
}

void HelloTriangleApp::CreateSwapChainImageViews()
{
	m_swapChainImageViews.resize(m_swapChainImages.size());

	for (size_t i = 0; i < m_swapChainImages.size(); ++i)
	{
		m_swapChainImageViews[i] = CreateImageView(m_swapChainImages[i], m_swapChainImageFormat, vk::ImageAspectFlagBits::eColor, 1);
	}
}

void HelloTriangleApp::CreateRenderPass()
{

	//Attachments are kind of like render targets
	auto colourAttachment = vk::AttachmentDescription()
		.setFormat(m_swapChainImageFormat)
		.setSamples(m_msaaSamples)
		.setLoadOp(vk::AttachmentLoadOp::eClear)//clears back buffer before drawing new frame?
		.setStoreOp(vk::AttachmentStoreOp::eStore)//rendered content will be stored to be read later (i.e. we'll see it on screen)
		.setStencilStoreOp(vk::AttachmentStoreOp::eDontCare)
		.setStencilLoadOp(vk::AttachmentLoadOp::eDontCare)
		.setInitialLayout(vk::ImageLayout::eUndefined)
		.setFinalLayout(vk::ImageLayout::eColorAttachmentOptimal);

	auto colourAttachmentRef = vk::AttachmentReference()
		.setAttachment(0) //referenced via index, we only have one currently so just grab the first
		.setLayout(vk::ImageLayout::eColorAttachmentOptimal);

	auto depthAttachment = vk::AttachmentDescription()
		.setFormat(FindDepthFormat())
		.setSamples(m_msaaSamples)
		.setLoadOp(vk::AttachmentLoadOp::eClear)
		.setStoreOp(vk::AttachmentStoreOp::eDontCare)
		.setStencilStoreOp(vk::AttachmentStoreOp::eDontCare)
		.setStencilLoadOp(vk::AttachmentLoadOp::eDontCare)
		.setInitialLayout(vk::ImageLayout::eUndefined)
		.setFinalLayout(vk::ImageLayout::eDepthStencilAttachmentOptimal);

	auto depthAttachmentRef = vk::AttachmentReference()
		.setAttachment(1)
		.setLayout(vk::ImageLayout::eDepthStencilAttachmentOptimal);

	auto colourAttachmentResolve = vk::AttachmentDescription()
		.setFormat(m_swapChainImageFormat)
		.setSamples(vk::SampleCountFlagBits::e1)
		.setLoadOp(vk::AttachmentLoadOp::eDontCare)
		.setStoreOp(vk::AttachmentStoreOp::eStore)
		.setStencilStoreOp(vk::AttachmentStoreOp::eDontCare)
		.setStencilLoadOp(vk::AttachmentLoadOp::eDontCare)
		.setInitialLayout(vk::ImageLayout::eUndefined)
		.setFinalLayout(vk::ImageLayout::ePresentSrcKHR);

	auto colourAttachmentResolveRef = vk::AttachmentReference()
		.setAttachment(2) 
		.setLayout(vk::ImageLayout::eColorAttachmentOptimal);

	auto subpass = vk::SubpassDescription()
		.setPipelineBindPoint(vk::PipelineBindPoint::eGraphics) //set automatically by constructor but I wanted to show it explicitly
		.setColorAttachmentCount(1)
		.setPColorAttachments(&colourAttachmentRef)
		.setPDepthStencilAttachment(&depthAttachmentRef)
		.setPResolveAttachments(&colourAttachmentResolveRef);

	auto dependency = vk::SubpassDependency()
		.setSrcSubpass(VK_SUBPASS_EXTERNAL)
		.setDstSubpass(0)
		.setSrcStageMask(vk::PipelineStageFlagBits::eColorAttachmentOutput)
		.setSrcAccessMask({})
		.setDstStageMask(vk::PipelineStageFlagBits::eColorAttachmentOutput)
		.setDstAccessMask(vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite);

	std::array<vk::AttachmentDescription, 3> attachments = { colourAttachment, depthAttachment, colourAttachmentResolve };

	auto renderPassInfo = vk::RenderPassCreateInfo()
		.setAttachmentCount(ToUI32(attachments.size()))
		.setPAttachments(attachments.data())
		.setSubpassCount(1)
		.setPSubpasses(&subpass)
		.setDependencyCount(1)
		.setPDependencies(&dependency);

	m_renderPass = m_logicalDevice.createRenderPass(renderPassInfo);
}

void HelloTriangleApp::CreateGraphicsPipeline()
{
	auto vertexShaderCode = ReadFile("Shaders/vert.spv");
	auto fragmentShaderCode = ReadFile("Shaders/frag.spv");

	//checks size of files, since char is the size of 1 byte, this equates to finding the size on disk
	//std::cout << "Vertex shader size: " << vertexShaderCode.size() << std::endl;
	//std::cout << "frag shader size: " << fragmentShaderCode.size() << std::endl;

	vk::ShaderModule vertShaderModule = CreateShaderModule(vertexShaderCode);
	vk::ShaderModule fragShaderModule = CreateShaderModule(fragmentShaderCode);

	auto vertShaderStageInfo = vk::PipelineShaderStageCreateInfo()
		.setStage(vk::ShaderStageFlagBits::eVertex)
		.setModule(vertShaderModule)
		.setPName("main");

	auto fragShaderStageInfo = vk::PipelineShaderStageCreateInfo()
		.setStage(vk::ShaderStageFlagBits::eFragment)
		.setModule(fragShaderModule)
		.setPName("main");

	vk::PipelineShaderStageCreateInfo shaderStages[] = { vertShaderStageInfo, fragShaderStageInfo };

	auto bindingDecriptions = Vertex::GetBindingDescription();
	auto attributeDescriptions = Vertex::GetAttributeDescriptions();

	auto vertexInputInfo = vk::PipelineVertexInputStateCreateInfo()
		.setVertexBindingDescriptionCount(1)
		.setPVertexBindingDescriptions(&bindingDecriptions)
		.setVertexAttributeDescriptionCount(static_cast<uint32_t>(attributeDescriptions.size()))
		.setPVertexAttributeDescriptions(attributeDescriptions.data());

	auto inputAssemblyCreateInfo = vk::PipelineInputAssemblyStateCreateInfo()
		.setTopology(vk::PrimitiveTopology::eTriangleList)
		.setPrimitiveRestartEnable(VK_FALSE);
	//vk::PrimitiveTopology::ePointList;

	auto viewport = vk::Viewport()
		.setX(0.0f)
		.setY(0.0f)
		.setWidth((float)m_swapChainExtent.width)
		.setHeight((float)m_swapChainExtent.height)
		.setMinDepth(0.0f)
		.setMaxDepth(1.0f);

	auto scissor = vk::Rect2D()
		.setOffset({ 0,0 })
		.setExtent(m_swapChainExtent);

	auto viewportStateCreateInfo = vk::PipelineViewportStateCreateInfo()
		.setViewportCount(1)
		.setPViewports(&viewport)
		.setScissorCount(1)
		.setPScissors(&scissor);

	auto rasteriserCreateInfo = vk::PipelineRasterizationStateCreateInfo()
		.setDepthClampEnable(VK_FALSE)
		.setRasterizerDiscardEnable(VK_FALSE)
		.setPolygonMode(vk::PolygonMode::eFill)
		.setLineWidth(1.0f)
		.setCullMode(vk::CullModeFlagBits::eBack)
		.setFrontFace(vk::FrontFace::eCounterClockwise)
		.setDepthBiasEnable(VK_FALSE)
		.setDepthBiasConstantFactor(0.0f)
		.setDepthBiasClamp(0.0f)
		.setDepthBiasSlopeFactor(0.0);

	auto multisamplingCreateInfo = vk::PipelineMultisampleStateCreateInfo()
		.setSampleShadingEnable(VK_FALSE)
		.setRasterizationSamples(m_msaaSamples)
		.setMinSampleShading(0.2f)
		.setPSampleMask(nullptr)
		.setAlphaToCoverageEnable(VK_FALSE)
		.setAlphaToOneEnable(VK_FALSE);

	auto depthStencilCreateInfo = vk::PipelineDepthStencilStateCreateInfo()
		.setDepthWriteEnable(VK_TRUE)
		.setDepthTestEnable(VK_TRUE)
		.setDepthCompareOp(vk::CompareOp::eLess)
		.setDepthBoundsTestEnable(VK_FALSE)
		.setStencilTestEnable(VK_FALSE);

	auto colourBlendAttachmentState = vk::PipelineColorBlendAttachmentState()
		.setColorWriteMask(vk::ColorComponentFlagBits::eR | vk::ColorComponentFlagBits::eG | vk::ColorComponentFlagBits::eB | vk::ColorComponentFlagBits::eA)
		.setBlendEnable(VK_FALSE)//are the following necessary, as no blending?
		.setSrcColorBlendFactor(vk::BlendFactor::eOne)
		.setDstColorBlendFactor(vk::BlendFactor::eZero)
		.setColorBlendOp(vk::BlendOp::eAdd)
		.setSrcAlphaBlendFactor(vk::BlendFactor::eOne)
		.setDstAlphaBlendFactor(vk::BlendFactor::eZero)
		.setAlphaBlendOp(vk::BlendOp::eAdd);

	auto colourBlendStateCreateInfo = vk::PipelineColorBlendStateCreateInfo()
		.setLogicOpEnable(VK_FALSE)
		.setLogicOp(vk::LogicOp::eCopy)
		.setAttachmentCount(1)
		.setPAttachments(&colourBlendAttachmentState)
		.setBlendConstants({ 0.0f, 0.0f,0.0f,0.0f });

	///////////////////// If I want to use /////////////////////////////////////
	////////////////////// DYNAMIC STATE ////////////////////////////////////////
	//vk::DynamicState dynamicStates[] = { vk::DynamicState::eViewport, vk::DynamicState::eLineWidth };

	//auto dynamicState = vk::PipelineDynamicStateCreateInfo()
	//	.setDynamicStateCount(2)
	//	.setPDynamicStates(dynamicStates);
	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////

	auto pipelineLayoutCreateInfo = vk::PipelineLayoutCreateInfo()
		.setSetLayoutCount(1)
		.setPSetLayouts(&m_descriptorSetLayout);

	
	if (m_logicalDevice.createPipelineLayout(&pipelineLayoutCreateInfo, nullptr, &m_pipelineLayout) != vk::Result::eSuccess)
	{
		throw std::runtime_error("failed to create pipeline layout");
	}

	auto pipelineCreateInfo = vk::GraphicsPipelineCreateInfo()
		.setStageCount(2)
		.setPStages(shaderStages)
		.setPVertexInputState(&vertexInputInfo)
		.setPInputAssemblyState(&inputAssemblyCreateInfo)
		.setPViewportState(&viewportStateCreateInfo)
		.setPRasterizationState(&rasteriserCreateInfo)
		.setPMultisampleState(&multisamplingCreateInfo)
		.setPDepthStencilState(&depthStencilCreateInfo)
		.setPColorBlendState(&colourBlendStateCreateInfo)
		.setPDynamicState(nullptr)
		.setLayout(m_pipelineLayout)
		.setRenderPass(m_renderPass)
		.setSubpass(0)
		.setBasePipelineHandle(nullptr)//used if we have a "parent" pipeline to get some functionality from
		.setBasePipelineIndex(-1);//used if we have a "parent" pipeline to get some functionality from

	m_graphicsPipeline = m_logicalDevice.createGraphicsPipeline(nullptr, pipelineCreateInfo);

	vkDestroyShaderModule(m_logicalDevice, vertShaderModule, nullptr);
	vkDestroyShaderModule(m_logicalDevice, fragShaderModule, nullptr);
}

void HelloTriangleApp::CreateFramebuffers()
{
	//m_swapChainFramebuffers.reserve(m_swapChainImageViews.size());

	for (auto imageView : m_swapChainImageViews) //tutorial uses regular for loop, I'm trying out range based, may have to change later
	{
		std::array<vk::ImageView, 3 > attachments = { m_colour->ImageView(), m_depth->ImageView(), imageView};

		auto framebufferCreateInfo = vk::FramebufferCreateInfo()
			.setRenderPass(m_renderPass)
			.setAttachmentCount(ToUI32(attachments.size()))
			.setPAttachments(attachments.data())
			.setWidth(m_swapChainExtent.width)
			.setHeight(m_swapChainExtent.height)
			.setLayers(1);

		m_swapChainFramebuffers.push_back(m_logicalDevice.createFramebuffer(framebufferCreateInfo));
	}
}

void HelloTriangleApp::CreateCommandPool()
{
	QueueFamilyIndices queueFamilyIndices = FindQueueFamilies(m_physicalDevice);

	auto commandPoolCreateInfo = vk::CommandPoolCreateInfo()
		.setQueueFamilyIndex(queueFamilyIndices.graphicsFamily.value());
		//.setFlags(vk::CommandPoolCreateFlagBits::??);
	
	m_commandPool = m_logicalDevice.createCommandPool(commandPoolCreateInfo);
}

void HelloTriangleApp::CreateCommandBuffers()
{
	m_commandBuffers.resize(m_swapChainFramebuffers.size());

	auto allocInfo = vk::CommandBufferAllocateInfo()
		.setCommandPool(m_commandPool)
		.setLevel(vk::CommandBufferLevel::ePrimary)
		.setCommandBufferCount((uint32_t)m_commandBuffers.size());

	m_commandBuffers = m_logicalDevice.allocateCommandBuffers(allocInfo);

	for (size_t i = 0; i < m_commandBuffers.size(); i++)
	{
		auto beginInfo = vk::CommandBufferBeginInfo()
			.setFlags(vk::CommandBufferUsageFlagBits::eSimultaneousUse);

		m_commandBuffers[i].begin(beginInfo);

		auto renderArea = vk::Rect2D()
			.setExtent(m_swapChainExtent)
			.setOffset({ 0,0 });

		std::array< vk::ClearValue, 2> clearValues = {};
		clearValues[0].color = vk::ClearColorValue();
		clearValues[1].depthStencil = {1.0f, 0 };


		auto renderPassInfo = vk::RenderPassBeginInfo()
			.setRenderPass(m_renderPass)
			.setFramebuffer(m_swapChainFramebuffers[i])
			.setRenderArea(renderArea)
			.setClearValueCount(ToUI32(clearValues.size()))
			.setPClearValues(clearValues.data());

		m_commandBuffers[i].beginRenderPass(renderPassInfo, vk::SubpassContents::eInline);
		m_commandBuffers[i].bindPipeline(vk::PipelineBindPoint::eGraphics, m_graphicsPipeline);
		m_commandBuffers[i].bindDescriptorSets(vk::PipelineBindPoint::eGraphics, m_pipelineLayout,0, m_descriptorSets[i] , nullptr);

		m_commandBuffers[i].bindVertexBuffers(0, m_vertexBuffer, { 0 }); //don't need to specify bind point as vertex/index buffers only used in graphics pipeline
		m_commandBuffers[i].bindIndexBuffer(m_indexBuffer, 0, vk::IndexType::eUint32);

		m_commandBuffers[i].drawIndexed(static_cast<uint32_t>(m_indices.size()), 1, 0, 0,0); //records the draw command, does not "call" it
		
		m_commandBuffers[i].endRenderPass();
		m_commandBuffers[i].end();
	}
}

void HelloTriangleApp::CreateSyncObjects()
{
	auto semaphoreCreateInfo = vk::SemaphoreCreateInfo();
	auto fenceCreateInfo = vk::FenceCreateInfo()
		.setFlags(vk::FenceCreateFlagBits::eSignaled);//create them with the signaled flag on so that the draw command doesn't wait forever

	for (int i = 0; i < MAX_FRAMES_IN_FLIGHT; ++i)
	{
		m_imageAvailableSemaphores.push_back(m_logicalDevice.createSemaphore(semaphoreCreateInfo));
		m_renderFinishedSemaphores.push_back(m_logicalDevice.createSemaphore(semaphoreCreateInfo));
		m_inFlightFences.push_back(m_logicalDevice.createFence(fenceCreateInfo));
	}
}

void HelloTriangleApp::CreateVertexBuffer()
{
	vk::DeviceSize bufferSize = sizeof(m_vertices[0])*m_vertices.size();

	vk::Buffer stagingBuffer;
	vk::DeviceMemory stagingBufferMem;

	CreateBuffer(bufferSize, vk::BufferUsageFlagBits::eTransferSrc, 
		vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, 
		stagingBuffer, stagingBufferMem);
	
	//void* data = m_logicalDevice.mapMemory(stagingBufferMem, 0, bufferSize);
	//memcpy(data, vertices.data(), (size_t)bufferSize);
	//m_logicalDevice.unmapMemory(stagingBufferMem);

	CopyBufferMemory(stagingBufferMem, m_vertices.data(), 0, bufferSize);

	CreateBuffer(bufferSize, vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eVertexBuffer,
		vk::MemoryPropertyFlagBits::eDeviceLocal,
		m_vertexBuffer, m_vertexBufferMem);

	CopyBuffer(stagingBuffer, m_vertexBuffer, bufferSize);
	
	m_logicalDevice.destroyBuffer(stagingBuffer);
	m_logicalDevice.freeMemory(stagingBufferMem);
}

void HelloTriangleApp::CreateIndexBuffer()
{
	vk::DeviceSize bufferSize = sizeof(m_indices[0]) * m_indices.size();

	vk::Buffer stagingBuffer;
	vk::DeviceMemory stagingBufferMem;

	CreateBuffer(bufferSize, vk::BufferUsageFlagBits::eTransferSrc,
		vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent,
		stagingBuffer, stagingBufferMem);

	//void* data = m_logicalDevice.mapMemory(stagingBufferMem, 0, bufferSize);
	//memcpy(data, indices.data(), (size_t)bufferSize);
	//m_logicalDevice.unmapMemory(stagingBufferMem);

	CopyBufferMemory(stagingBufferMem, m_indices.data(), 0, bufferSize);

	CreateBuffer(bufferSize, vk::BufferUsageFlagBits::eTransferDst | vk::BufferUsageFlagBits::eIndexBuffer,
		vk::MemoryPropertyFlagBits::eDeviceLocal,
		m_indexBuffer, m_indexBufferMem);

	CopyBuffer(stagingBuffer, m_indexBuffer, bufferSize);

	m_logicalDevice.destroyBuffer(stagingBuffer);
	m_logicalDevice.freeMemory(stagingBufferMem);
}

void HelloTriangleApp::CreateDescriptorSetLayout()
{
	auto uboLayoutBinding = vk::DescriptorSetLayoutBinding()
		.setBinding(0) //matches the binding value used in shader.vert
		.setDescriptorType(vk::DescriptorType::eUniformBuffer)
		.setDescriptorCount(1)
		.setStageFlags(vk::ShaderStageFlagBits::eVertex) //referenced by vertex shader stage, not quite as global as DX ConstantBuffer?
		.setPImmutableSamplers(nullptr);

	auto samplerLayoutBinding = vk::DescriptorSetLayoutBinding()
		.setBinding(1) //matches the binding value used in shader.vert
		.setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
		.setDescriptorCount(1)
		.setStageFlags(vk::ShaderStageFlagBits::eFragment)
		.setPImmutableSamplers(nullptr);

	std::array<vk::DescriptorSetLayoutBinding, 2> bindings = { uboLayoutBinding, samplerLayoutBinding };

	auto descriptorSetLayoutCreateInfo = vk::DescriptorSetLayoutCreateInfo()
		.setBindingCount(static_cast<uint32_t>(bindings.size()) )
		.setPBindings(bindings.data());

	m_descriptorSetLayout = m_logicalDevice.createDescriptorSetLayout(descriptorSetLayoutCreateInfo);
}

void HelloTriangleApp::CreateUniformBuffers()
{
	vk::DeviceSize bufferSize = sizeof(UniformBufferObject);

	m_uniformBuffers.resize(m_swapChainImages.size());
	m_uniformBuffersMemory.resize(m_swapChainImages.size());

	for (size_t i = 0; i < m_swapChainImages.size(); ++i)
	{
		CreateBuffer(bufferSize, vk::BufferUsageFlagBits::eUniformBuffer, 
			vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent, 
			m_uniformBuffers[i], m_uniformBuffersMemory[i]);
	}
}

void HelloTriangleApp::CreateDescriptorPool()
{
	auto descriptorPoolSizeUbo = vk::DescriptorPoolSize()
		.setType(vk::DescriptorType::eUniformBuffer)
		.setDescriptorCount(static_cast<uint32_t>(m_swapChainImages.size()));

	auto descriptorPoolSizeComboImageSampler = vk::DescriptorPoolSize()
		.setType(vk::DescriptorType::eCombinedImageSampler)
		.setDescriptorCount(static_cast<uint32_t>(m_swapChainImages.size()));

	std::array< vk::DescriptorPoolSize, 2> descriptorPoolSizes = { descriptorPoolSizeUbo, descriptorPoolSizeComboImageSampler };

	auto descriptorPoolCreateInfo = vk::DescriptorPoolCreateInfo()
		.setPoolSizeCount(ToUI32(descriptorPoolSizes.size()))
		.setPPoolSizes(descriptorPoolSizes.data())
		.setMaxSets(ToUI32(m_swapChainImages.size()));

	m_descriptorPool = m_logicalDevice.createDescriptorPool(descriptorPoolCreateInfo);
}

void HelloTriangleApp::CreateDescriptorSets()
{
	std::vector<vk::DescriptorSetLayout> layouts(m_swapChainImages.size(), m_descriptorSetLayout);

	const auto descriptorSetAllocInfo = vk::DescriptorSetAllocateInfo()
		.setDescriptorPool(m_descriptorPool)
		.setDescriptorSetCount(static_cast<uint32_t>(m_swapChainImages.size()))
		.setPSetLayouts(layouts.data());

	//m_descriptorSets.resize(m_swapChainImages.size());

	m_descriptorSets = m_logicalDevice.allocateDescriptorSets(descriptorSetAllocInfo);

	for (size_t i = 0; i < m_swapChainImages.size(); ++i)
	{
		auto bufferInfo = vk::DescriptorBufferInfo()
			.setBuffer(m_uniformBuffers[i])
			.setOffset(0)
			.setRange(sizeof(UniformBufferObject));

		auto imageInfo = vk::DescriptorImageInfo()
			.setImageLayout(vk::ImageLayout::eShaderReadOnlyOptimal)
			.setImageView(m_texture->ImageView())
			.setSampler(m_textureSampler);

		auto writeDescriptorSetUbo = vk::WriteDescriptorSet()
			.setDstSet(m_descriptorSets[i])
			.setDstBinding(0) //matches the "binding" of the uniform buffer, as seen in vert.shader and buffer description here in c++
			.setDstArrayElement(0)
			.setDescriptorCount(1)
			.setDescriptorType(vk::DescriptorType::eUniformBuffer)
			.setPBufferInfo(&bufferInfo);

		auto writeDescriptorSetComboImageSampler = vk::WriteDescriptorSet()
			.setDstSet(m_descriptorSets[i])
			.setDstBinding(1)
			.setDstArrayElement(0)
			.setDescriptorCount(1)
			.setDescriptorType(vk::DescriptorType::eCombinedImageSampler)
			.setPImageInfo(&imageInfo);

		std::array<vk::WriteDescriptorSet, 2> writeDescriptorSets = { writeDescriptorSetUbo, writeDescriptorSetComboImageSampler };

		m_logicalDevice.updateDescriptorSets(writeDescriptorSets, nullptr);

	}
}

void HelloTriangleApp::CreateTextureImage()
{
	int texWidth, texHeight, texChannels;

	stbi_uc* pixels = stbi_load(TEXTURE_PATH.c_str(), &texWidth, &texHeight, &texChannels, STBI_rgb_alpha);
	
	vk::DeviceSize imageSize = texWidth * texHeight * 4;

	m_mipLevels = ToUI32(std::floor(std::log2(std::max(texWidth, texHeight)))) + 1;

	if (!pixels)
	{
		throw std::runtime_error("failed to load texture image");
	}

	vk::Buffer stagingBuffer;
	vk::DeviceMemory stagingBufferMemory;

	CreateBuffer(imageSize, vk::BufferUsageFlagBits::eTransferSrc, vk::MemoryPropertyFlagBits::eHostVisible | vk::MemoryPropertyFlagBits::eHostCoherent,
		stagingBuffer, stagingBufferMemory);

	CopyBufferMemory(stagingBufferMemory, pixels, 0, imageSize);

	stbi_image_free(pixels);
	
	m_texture = std::make_unique<Texture>(m_logicalDevice, m_physicalDevice, m_msaaSamples);

	m_texture->CreateImage(texWidth, texHeight, m_mipLevels, vk::SampleCountFlagBits::e1,
		vk::Format::eR8G8B8A8Unorm, vk::ImageTiling::eOptimal,
		vk::ImageUsageFlagBits::eTransferSrc | vk::ImageUsageFlagBits::eTransferDst | vk::ImageUsageFlagBits::eSampled, 
		vk::MemoryPropertyFlagBits::eDeviceLocal);

	//TODO: Work out whether I can batch these command buffer things together,
	//it feels like I should probably start recording here and end recording, then submit after generating mip maps
	auto commandBuffer = BeginSingleTimeCommands();
	m_texture->TransitionImageLayout(vk::Format::eR8G8B8A8Unorm, commandBuffer,vk::ImageLayout::eUndefined, vk::ImageLayout::eTransferDstOptimal, m_mipLevels);
	EndSingleTimeCommands(commandBuffer);

	auto commandBuffer1 = BeginSingleTimeCommands();
	m_texture->CopyBufferToImage(stagingBuffer, commandBuffer1, static_cast<uint32_t>(texWidth), static_cast<uint32_t>(texHeight));
	EndSingleTimeCommands(commandBuffer1);

	//TransitionImageLayout(m_textureImage, vk::Format::eR8G8B8A8Unorm, vk::ImageLayout::eTransferDstOptimal, vk::ImageLayout::eShaderReadOnlyOptimal, m_mipLevels);

	m_logicalDevice.destroyBuffer(stagingBuffer);
	m_logicalDevice.freeMemory(stagingBufferMemory);

	auto commandBuffer2 = BeginSingleTimeCommands();
	m_texture->GenerateMipmaps(vk::Format::eR8G8B8A8Unorm,commandBuffer2, texWidth, texHeight, m_mipLevels);
	EndSingleTimeCommands(commandBuffer2);
}

void HelloTriangleApp::CreateTextureImageView()
{
	m_texture->CreateImageView(vk::Format::eR8G8B8A8Unorm, vk::ImageAspectFlagBits::eColor, m_mipLevels);
}

void HelloTriangleApp::CreateTextureSampler()
{
	auto samplerInfo = vk::SamplerCreateInfo()
		.setMagFilter(vk::Filter::eLinear)
		.setMinFilter(vk::Filter::eLinear)
		.setAddressModeU(vk::SamplerAddressMode::eRepeat)
		.setAddressModeV(vk::SamplerAddressMode::eRepeat)
		.setAddressModeW(vk::SamplerAddressMode::eRepeat)
		.setAnisotropyEnable(VK_TRUE)
		.setMaxAnisotropy(16)
		.setBorderColor(vk::BorderColor::eIntOpaqueBlack)
		.setUnnormalizedCoordinates(VK_FALSE)
		.setCompareEnable(VK_FALSE)
		.setCompareOp(vk::CompareOp::eAlways)
		.setMipmapMode(vk::SamplerMipmapMode::eLinear)
		.setMipLodBias(0.0f)
		.setMinLod(0.0f)
		.setMaxLod(static_cast<float>(m_mipLevels));

	m_textureSampler = m_logicalDevice.createSampler(samplerInfo);

}

void HelloTriangleApp::CreateDepthResources()
{
	auto depthFormat = FindDepthFormat();

	m_depth = std::make_unique<Texture>(m_logicalDevice, m_physicalDevice, m_msaaSamples);

	m_depth->CreateImage(m_swapChainExtent.width, m_swapChainExtent.height, 1, m_msaaSamples,
		depthFormat, vk::ImageTiling::eOptimal,
		vk::ImageUsageFlagBits::eDepthStencilAttachment, vk::MemoryPropertyFlagBits::eDeviceLocal);

	m_depth->CreateImageView(depthFormat, vk::ImageAspectFlagBits::eDepth, 1);

	auto commandBuffer = BeginSingleTimeCommands();
	m_depth->TransitionImageLayout(depthFormat, commandBuffer, vk::ImageLayout::eUndefined, vk::ImageLayout::eDepthStencilAttachmentOptimal, 1);
	EndSingleTimeCommands(commandBuffer);
}

void HelloTriangleApp::CreateColourResources()
{
	auto colourFormat = m_swapChainImageFormat;

	m_colour = std::make_unique<Texture>(m_logicalDevice, m_physicalDevice, m_msaaSamples);

	m_colour->CreateImage(m_swapChainExtent.width, m_swapChainExtent.height, 1, m_msaaSamples, colourFormat, vk::ImageTiling::eOptimal,
		vk::ImageUsageFlagBits::eTransientAttachment | vk::ImageUsageFlagBits::eColorAttachment, vk::MemoryPropertyFlagBits::eDeviceLocal);

	m_colour->CreateImageView(colourFormat, vk::ImageAspectFlagBits::eColor, 1);

	auto commandBuffer = BeginSingleTimeCommands();
	m_colour->TransitionImageLayout(colourFormat, commandBuffer, vk::ImageLayout::eUndefined, vk::ImageLayout::eColorAttachmentOptimal, 1);
	EndSingleTimeCommands(commandBuffer);
}

void HelloTriangleApp::LoadModel()
{
	tinyobj::attrib_t attributes;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;
	std::string warning, error;

	if (!tinyobj::LoadObj(&attributes, &shapes, &materials, &warning, &error, MODEL_PATH.c_str()))
	{
		throw std::runtime_error(warning + error);
	}

	std::unordered_map<Vertex, uint32_t> uniqueVertices = {};

	for (const auto& shape : shapes)
	{
		for (const auto& index : shape.mesh.indices)
		{
			Vertex vertex = {};

			vertex.pos =
			{
				attributes.vertices[3 * index.vertex_index + 0],
				attributes.vertices[3 * index.vertex_index + 1],
				attributes.vertices[3 * index.vertex_index + 2]
			};

			vertex.texCoord =
			{
				attributes.texcoords[2 * index.texcoord_index + 0],
				1.0f - attributes.texcoords[2 * index.texcoord_index + 1]
			};

			vertex.colour = { 1.0f,1.0f,1.0f };

			if (uniqueVertices.count(vertex) == 0)
			{
				uniqueVertices[vertex] = ToUI32(m_vertices.size());
				m_vertices.push_back(vertex);
			}

			m_indices.push_back(uniqueVertices[vertex]);
		}
	}
}

void HelloTriangleApp::RecreateSwapChain()
{
	int width = 0, height = 0;
	while (width == 0 || height == 0)
	{
		glfwGetFramebufferSize(m_window, &width, &height);
		glfwWaitEvents();
	}

	m_logicalDevice.waitIdle();

	CleanUpSwapChain();

	CreateSwapChain();
	CreateSwapChainImageViews();
	CreateRenderPass();
	CreateGraphicsPipeline();
	CreateColourResources();
	CreateDepthResources();
	CreateFramebuffers();
	CreateUniformBuffers();
	CreateDescriptorPool();
	CreateDescriptorSets();
	CreateCommandBuffers();
}

void HelloTriangleApp::CleanUpSwapChain()
{
	m_colour->CleanUp();
	m_depth->CleanUp();

	for (auto framebuffer : m_swapChainFramebuffers)
	{
		m_logicalDevice.destroyFramebuffer(framebuffer);
	}
	m_swapChainFramebuffers.clear();

	m_logicalDevice.freeCommandBuffers(m_commandPool, m_commandBuffers);

	m_logicalDevice.destroyPipeline(m_graphicsPipeline);
	m_logicalDevice.destroyPipelineLayout(m_pipelineLayout);
	m_logicalDevice.destroyRenderPass(m_renderPass);

	for (auto imageView : m_swapChainImageViews)
	{
		m_logicalDevice.destroyImageView(imageView);
	}
	m_swapChainImageViews.clear();

	m_logicalDevice.destroySwapchainKHR(m_swapChain);

	for (size_t i=0; i< m_swapChainImages.size(); ++i)
	{
		m_logicalDevice.destroyBuffer(m_uniformBuffers[i]);
		m_logicalDevice.freeMemory(m_uniformBuffersMemory[i]);
	}
	m_uniformBuffers.clear();
	m_uniformBuffersMemory.clear();

	m_logicalDevice.destroyDescriptorPool(m_descriptorPool);
}

void HelloTriangleApp::DrawFrame()
{
	//acquire image from swap chain
	//execute command buffer with that image as attachment in the framebuffer
	//return image to swap chain for presentation
	m_logicalDevice.waitForFences(m_inFlightFences[m_currentFrame], VK_TRUE, std::numeric_limits<uint64_t>::max());
	if (m_hasFramebufferBeenResized)
	{
		int i = 0;
	}

	uint32_t imageIndex;

	auto result = m_logicalDevice.acquireNextImageKHR(m_swapChain, std::numeric_limits<uint64_t>::max(), m_imageAvailableSemaphores[m_currentFrame], nullptr, &imageIndex);

	if (result == vk::Result::eErrorOutOfDateKHR)
	{
		RecreateSwapChain();
		return;
	}
	else if (result != vk::Result::eSuccess && result != vk::Result::eSuboptimalKHR)
	{
		throw std::runtime_error("failed to acquire image from swap chain");
	}

	UpdateUniformBuffer(imageIndex);

	vk::Semaphore waitSemaphores[] = { m_imageAvailableSemaphores[m_currentFrame] };
	vk::Semaphore signalSemaphores[] = { m_renderFinishedSemaphores[m_currentFrame] };

	vk::PipelineStageFlags waitStages[] = { vk::PipelineStageFlagBits::eColorAttachmentOutput };

	auto submitInfo = vk::SubmitInfo()
		.setWaitSemaphoreCount(1)
		.setPWaitSemaphores(waitSemaphores)
		.setPWaitDstStageMask(waitStages)
		.setCommandBufferCount(1).setPCommandBuffers(&m_commandBuffers[imageIndex])
		.setSignalSemaphoreCount(1)
		.setPSignalSemaphores(signalSemaphores); //will signal these semaphores when execution has finished

	m_logicalDevice.resetFences(m_inFlightFences[m_currentFrame]);

	m_graphicsQueue.submit( submitInfo, m_inFlightFences[m_currentFrame] );//takes an array of SubmitInfos, however vulkan.hpp allows a single value to be passed through

	vk::SwapchainKHR swapChains[] = { m_swapChain };

	auto presentInfo = vk::PresentInfoKHR()
		.setWaitSemaphoreCount(1)
		.setPWaitSemaphores(signalSemaphores)
		.setSwapchainCount(1)
		.setPSwapchains(swapChains)
		.setPImageIndices(&imageIndex);

	if (m_hasFramebufferBeenResized)
	{
		m_swapChainFramebuffers;
		int i = 0;

	}

	result = m_presentQueue.presentKHR(&presentInfo);
	
	if (result != vk::Result::eSuccess)
	{
	}

	if (result == vk::Result::eErrorOutOfDateKHR || result == vk::Result::eSuboptimalKHR || m_hasFramebufferBeenResized)
	{
		RecreateSwapChain();
		m_hasFramebufferBeenResized = false;

	}
	else if (result != vk::Result::eSuccess)
	{
		throw std::runtime_error("failed to present swap chain image");
	}

	m_currentFrame = (m_currentFrame + 1) % MAX_FRAMES_IN_FLIGHT;
}

void HelloTriangleApp::CreateBuffer(vk::DeviceSize size, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties, vk::Buffer& buffer, vk::DeviceMemory& bufferMemory)
{
	auto bufferInfo = vk::BufferCreateInfo()
		.setSize(size)
		.setUsage(usage)
		.setSharingMode(vk::SharingMode::eExclusive);

	buffer = m_logicalDevice.createBuffer(bufferInfo);

	vk::MemoryRequirements memRequirements = m_logicalDevice.getBufferMemoryRequirements(buffer);

	auto memAllocInfo = vk::MemoryAllocateInfo()
		.setAllocationSize(memRequirements.size)
		.setMemoryTypeIndex(FindMemoryType(memRequirements.memoryTypeBits, properties));

	bufferMemory = m_logicalDevice.allocateMemory(memAllocInfo);

	m_logicalDevice.bindBufferMemory(buffer, bufferMemory, 0);
}

void HelloTriangleApp::CopyBuffer(vk::Buffer source, vk::Buffer destination, vk::DeviceSize size)
{
	auto commandBuffer = BeginSingleTimeCommands();

	auto copyRegion = vk::BufferCopy()
		.setSrcOffset(0)
		.setDstOffset(0)
		.setSize(size);

	commandBuffer.copyBuffer(source, destination, copyRegion);

	EndSingleTimeCommands(commandBuffer);
}

void HelloTriangleApp::UpdateUniformBuffer(uint32_t currentImage)
{
	static auto startTime = std::chrono::high_resolution_clock::now();

	auto currentTime = std::chrono::high_resolution_clock::now();

	float time = std::chrono::duration<float, std::chrono::seconds::period>(currentTime - startTime).count();

	UniformBufferObject ubo = {};

	ubo.model = glm::rotate(glm::mat4(1.0f), time * glm::radians(90.0f), glm::vec3(0.0f, 0.0f, 1.0f));

	ubo.view = glm::lookAt(glm::vec3(2.0f, 2.0f, 2.0f), glm::vec3(0.0f), glm::vec3(0.0f, 0.0f, 1.0f));

	ubo.proj = glm::perspective(glm::radians(45.0f), m_swapChainExtent.width / (float)m_swapChainExtent.height, 0.1f, 10.0f);

	ubo.proj[1][1] *= -1;//glm was made for OpenGL which uses an inverted Y axis, so need to flip it manually here;

	//void* data = m_logicalDevice.mapMemory(m_uniformBuffersMemory[currentImage], 0, sizeof(ubo));
	//memcpy(data, &ubo, sizeof(ubo));
	//m_logicalDevice.unmapMemory(m_uniformBuffersMemory[currentImage]);

	CopyBufferMemory(m_uniformBuffersMemory[currentImage], &ubo, 0, sizeof(ubo));
}

void HelloTriangleApp::CopyBufferMemory(vk::DeviceMemory deviceMem, const void* dataBeingCopied, vk::DeviceSize offset, vk::DeviceSize size, vk::MemoryMapFlags flags)
{
	void* data = m_logicalDevice.mapMemory(deviceMem, offset, size, flags);
	memcpy(data, dataBeingCopied, size);
	m_logicalDevice.unmapMemory(deviceMem);
}

void HelloTriangleApp::FramebufferResizeCallBack(GLFWwindow* window, int width, int height)
{
	auto app = reinterpret_cast<HelloTriangleApp*>(glfwGetWindowUserPointer(window));
	app->m_hasFramebufferBeenResized = true;
}
//--------------------------//
//------MAIN FUNCTION-------//
//--------------------------//

int main()
{
	//HelloTriangleApp app;
	std::unique_ptr<HelloTriangleApp> app = std::make_unique<HelloTriangleApp>();

	try
	{
		app->Run();
		//app.Run();
	}
	catch (const std::exception& e)
	{
		std::cerr << e.what() << std::endl;
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}
