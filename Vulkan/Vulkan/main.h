#pragma once
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#include <iostream>
#include <stdexcept>
#include <functional>
#include <cstdlib>
#include <memory>
#include <optional>
#include "DebugFunctions.h"
#include <fstream>
#include <vulkan/vulkan.hpp>
#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <chrono>

#define STB_IMAGE_IMPLEMENTATION
#include <stb_image.h>

#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>

#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/hash.hpp>
#include "Texture.h"

#define ToUI32(type) static_cast<uint32_t>(type)

const std::vector<const char*> g_validationLayers = { "VK_LAYER_KHRONOS_validation" };
const std::vector<const char*> g_deviceExtensions = { VK_KHR_SWAPCHAIN_EXTENSION_NAME };

#ifdef NDEBUG
const bool g_validationLayersEnabled = false;
#else
const bool g_validationLayersEnabled = true;
#endif // NDEBUG

namespace
{
	const int WIDTH = 800;
	const int HEIGHT = 600;

	const std::string MODEL_PATH = "Models/chalet.obj";
	const std::string TEXTURE_PATH = "Textures/chalet.jpg";

	const int MAX_FRAMES_IN_FLIGHT = 2;

	struct UniformBufferObject 
	{//beware alignment requirements https://www.khronos.org/registry/vulkan/specs/1.1-extensions/html/chap14.html#interfaces-resources-layout
		//https://vulkan-tutorial.com/Uniform_buffers/Descriptor_pool_and_sets
		alignas(16) glm::mat4 model;
		alignas(16) glm::mat4 view;
		alignas(16) glm::mat4 proj; //useful to be explicit about the alignment even though this setup wouldn't have had any alignment issues
	};

	struct Vertex
	{
		glm::vec3 pos;
		glm::vec3 colour;
		glm::vec2 texCoord;

		static vk::VertexInputBindingDescription GetBindingDescription()
		{
			auto bindingDesc = vk::VertexInputBindingDescription()
				.setBinding(0)
				.setStride(sizeof(Vertex))
				.setInputRate(vk::VertexInputRate::eVertex);

			return bindingDesc;
		}

		static auto GetAttributeDescriptions()
		{
			auto attributeDesc = std::array < vk::VertexInputAttributeDescription, 3>();
			attributeDesc[0]
				.setBinding(0)
				.setLocation(0) //maps to "location" in shader, eg layout(location = 0) in vec2 inPosition; 
				.setFormat(vk::Format::eR32G32B32Sfloat)
				.setOffset(offsetof(Vertex, pos));

			attributeDesc[1]
				.setBinding(0)
				.setLocation(1)
				.setFormat(vk::Format::eR32G32B32Sfloat)
				.setOffset(offsetof(Vertex, colour));

			attributeDesc[2]
				.setBinding(0)
				.setLocation(2)
				.setFormat(vk::Format::eR32G32Sfloat)
				.setOffset(offsetof(Vertex, texCoord));

			return attributeDesc;
		}

		bool operator==(const Vertex& other) const
		{
			return pos == other.pos && colour == other.colour && texCoord == other.texCoord;
		}
	};

}

namespace std
{
	template<> struct hash<Vertex>
	{
		size_t operator()(Vertex const& vertex) const 
		{
			return (
				(hash<glm::vec3>()(vertex.pos) ^ 
				(hash<glm::vec3>()(vertex.colour) << 1)) >> 1) ^ 
				(hash<glm::vec2>()(vertex.texCoord) << 1
				);
		}
	};
}

struct QueueFamilyIndices
{
	std::optional<uint32_t> graphicsFamily;
	std::optional<uint32_t> presentFamily;

	bool IsComplete()
	{
		return graphicsFamily.has_value() && presentFamily.has_value();
	}
};

struct SwapChainSupportDetails
{
	vk::SurfaceCapabilitiesKHR capabilities;
	std::vector<vk::SurfaceFormatKHR> formats;
	std::vector<vk::PresentModeKHR> presentModes;

};

static VKAPI_ATTR VkBool32 VKAPI_CALL debugCallback(
	VkDebugUtilsMessageSeverityFlagBitsEXT messageSeverity,
	VkDebugUtilsMessageTypeFlagsEXT messageType,
	const VkDebugUtilsMessengerCallbackDataEXT* pCallbackData,
	void* pUserData)
{
	std::cerr << "Validation layers: " << pCallbackData->pMessage << std::endl;

	return VK_FALSE;
}

static std::vector<char> ReadFile(const std::string& fileName)
{
	std::ifstream file(fileName, std::ios::ate | std::ios::binary);

	if (!file.is_open())
	{
		throw std::runtime_error("failed to open file");
	}

	size_t fileSize = (size_t)file.tellg();
	std::vector<char> buffer(fileSize);

	file.seekg(0);
	file.read(buffer.data(), fileSize);

	file.close();

	return buffer;
}


class HelloTriangleApp
{
public:
	HelloTriangleApp();
	~HelloTriangleApp();
	void Run();



private:
	static void FramebufferResizeCallBack(GLFWwindow* window, int width, int height);

	void InitWindow();
	void InitVulkan();
	void MainLoop();
	void CleanUp();

	void CreateInstance();
	void SetupDebugMessenger();
	void PickPhysicalDevice();
	void CreateLogicalDevice();
	void CreateSurface();
	void CreateSwapChain();
	vk::ImageView CreateImageView(vk::Image image, vk::Format format, vk::ImageAspectFlags aspectFlags, uint32_t mipLevels);
	void CreateSwapChainImageViews();
	void CreateRenderPass();
	void CreateGraphicsPipeline();
	void CreateFramebuffers();
	void CreateCommandPool();
	void CreateCommandBuffers();
	void CreateSyncObjects();
	void CreateVertexBuffer();
	void CreateIndexBuffer();
	void CreateDescriptorSetLayout();
	void CreateUniformBuffers();
	void CreateDescriptorPool();
	void CreateDescriptorSets();
	void CreateTextureImage();
	void CreateTextureImageView();
	void CreateTextureSampler();
	void CreateDepthResources();
	void CreateColourResources();
	void LoadModel();

	void RecreateSwapChain();
	void CleanUpSwapChain();
	
	void DrawFrame();//called from main loop

	void CreateBuffer(vk::DeviceSize size, vk::BufferUsageFlags usage, vk::MemoryPropertyFlags properties, vk::Buffer& buffer, vk::DeviceMemory& bufferMemory);
	void CopyBuffer(vk::Buffer source, vk::Buffer destination, vk::DeviceSize size);

	void CopyBufferMemory(vk::DeviceMemory deviceMem, const void* dataBeingCopied, vk::DeviceSize offset, vk::DeviceSize size, vk::MemoryMapFlags flags = vk::MemoryMapFlags());
	void UpdateUniformBuffer(uint32_t currentImage);
	
	vk::CommandBuffer BeginSingleTimeCommands();
	void EndSingleTimeCommands(vk::CommandBuffer commandBuffer);

	vk::SampleCountFlagBits GetMaxUsableSampleCount();//I had trouble using the hpp version :(
	bool IsPhysicalDeviceSuitable(vk::PhysicalDevice);
	bool CheckValidationLayerSupport();
	bool CheckDeviceExtensionSupport(vk::PhysicalDevice device);

	void PopulateDebugMessengerCreateInfo(VkDebugUtilsMessengerCreateInfoEXT& createInfo);
	QueueFamilyIndices FindQueueFamilies(vk::PhysicalDevice device);
	SwapChainSupportDetails QuerySwapChainSupport(vk::PhysicalDevice device);
	vk::SurfaceFormatKHR ChooseSwapSurfaceFormat(const std::vector<vk::SurfaceFormatKHR>& availableFormats);
	vk::PresentModeKHR ChooseSwapPresentMode(const std::vector<vk::PresentModeKHR>& availablePresentModes);
	vk::Extent2D ChooseSwapExtent(const vk::SurfaceCapabilitiesKHR& capabilties);

	vk::ShaderModule CreateShaderModule(const std::vector<char>& code);
	uint32_t FindMemoryType(uint32_t typeFilter, vk::MemoryPropertyFlags properties);

	std::vector<const char*> GetRequiredExtensions();
	vk::Format FindSupportedFormat(const std::vector<vk::Format>& candidates, vk::ImageTiling tiling, vk::FormatFeatureFlags features);
	vk::Format FindDepthFormat();

	GLFWwindow* m_window;
	vk::Instance m_instance;
	VkDebugUtilsMessengerEXT m_debugMessenger;
	vk::PhysicalDevice m_physicalDevice;
	vk::Device m_logicalDevice;
	vk::Queue m_graphicsQueue;
	vk::SurfaceKHR m_surface;
	vk::Queue m_presentQueue;
	vk::SwapchainKHR m_swapChain;

	std::vector<vk::Image> m_swapChainImages;
	std::vector<vk::ImageView> m_swapChainImageViews;
	vk::Format m_swapChainImageFormat;

	vk::Extent2D m_swapChainExtent;
	vk::RenderPass m_renderPass;
	vk::DescriptorSetLayout m_descriptorSetLayout;
	vk::PipelineLayout m_pipelineLayout;
	vk::Pipeline m_graphicsPipeline;
	std::vector<vk::Framebuffer> m_swapChainFramebuffers;
	vk::CommandPool m_commandPool;
	std::vector<vk::CommandBuffer> m_commandBuffers;

	std::vector<vk::Semaphore> m_imageAvailableSemaphores;
	std::vector<vk::Semaphore> m_renderFinishedSemaphores;
	std::vector<vk::Fence> m_inFlightFences;
	size_t m_currentFrame = 0;
	bool m_hasFramebufferBeenResized = false;

	vk::Buffer m_vertexBuffer;
	vk::DeviceMemory m_vertexBufferMem;
	vk::Buffer m_indexBuffer;
	vk::DeviceMemory m_indexBufferMem;

	std::vector<vk::Buffer> m_uniformBuffers;
	std::vector<vk::DeviceMemory> m_uniformBuffersMemory;

	vk::DescriptorPool m_descriptorPool;
	std::vector<vk::DescriptorSet> m_descriptorSets;

	uint32_t m_mipLevels;

	std::unique_ptr<Texture> m_texture;
	vk::Sampler m_textureSampler;

	std::unique_ptr<Texture> m_depth;

	vk::SampleCountFlagBits m_msaaSamples = vk::SampleCountFlagBits::e1;
	std::unique_ptr<Texture> m_colour;

	std::vector<Vertex> m_vertices;
	std::vector<uint32_t> m_indices;
};



