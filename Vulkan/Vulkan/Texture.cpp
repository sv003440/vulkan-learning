#include "Texture.h"

Texture::~Texture()
{
	
}

void Texture::CleanUp()
{
	if (m_logicalDevice)
	{
		m_logicalDevice.destroyImageView(m_imageView);
		m_logicalDevice.destroyImage(m_image);
		m_logicalDevice.freeMemory(m_imageMemory);
	}
	
}

//don't forget to call BeginSingleTimeCommands before this and EndSingleTimeCommands after this
void Texture::CopyBufferToImage(const vk::Buffer& buffer, vk::CommandBuffer commandBuffer, const uint32_t width, const uint32_t height)
{
	auto imageSubresourceLayers = vk::ImageSubresourceLayers()
		.setBaseArrayLayer(0)
		.setAspectMask(vk::ImageAspectFlagBits::eColor)
		.setMipLevel(0)
		.setLayerCount(1);

	auto regionToCopy = vk::BufferImageCopy()
		.setBufferOffset(0)
		.setBufferRowLength(0)
		.setBufferImageHeight(0)
		.setImageSubresource(imageSubresourceLayers)
		.setImageOffset({ 0,0,0 })
		.setImageExtent({ width,height,1 });

	commandBuffer.copyBufferToImage(buffer, m_image, vk::ImageLayout::eTransferDstOptimal, regionToCopy);
}

void Texture::CreateImage(uint32_t width, uint32_t height, uint32_t mipLevels, vk::SampleCountFlagBits numSamples, vk::Format format, 
	vk::ImageTiling tiling, vk::ImageUsageFlags usage,	vk::MemoryPropertyFlags properties)
{
	auto imageInfo = vk::ImageCreateInfo()
		.setImageType(vk::ImageType::e2D)
		.setExtent({ static_cast<uint32_t>(width) , static_cast<uint32_t>(height), 1 })
		.setMipLevels(mipLevels)
		.setArrayLayers(1)
		.setFormat(format)
		.setTiling(tiling)
		.setInitialLayout(vk::ImageLayout::eUndefined)
		.setUsage(usage)
		.setSharingMode(vk::SharingMode::eExclusive)
		.setSamples(numSamples);

	m_image = m_logicalDevice.createImage(imageInfo);

	vk::MemoryRequirements memoryRequirements = m_logicalDevice.getImageMemoryRequirements(m_image);

	auto memoryAllocInfo = vk::MemoryAllocateInfo()
		.setAllocationSize(memoryRequirements.size)
		.setMemoryTypeIndex(FindMemoryType(memoryRequirements.memoryTypeBits, properties));

	m_imageMemory = m_logicalDevice.allocateMemory(memoryAllocInfo);

	m_logicalDevice.bindImageMemory(m_image, m_imageMemory, 0);
}

//don't forget to call BeginSingleTimeCommands before this and EndSingleTimeCommands after this
void Texture::TransitionImageLayout(vk::Format format, vk::CommandBuffer commandBuffer,vk::ImageLayout oldLayout, vk::ImageLayout newLayout, uint32_t mipLevels)
{
	auto subresourceRange = vk::ImageSubresourceRange()
		.setBaseMipLevel(0)
		.setLevelCount(mipLevels)
		.setBaseArrayLayer(0)
		.setLayerCount(1)
		.setAspectMask(vk::ImageAspectFlagBits::eColor);

	auto barrier = vk::ImageMemoryBarrier()
		.setOldLayout(oldLayout)
		.setNewLayout(newLayout)
		.setSrcQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
		.setDstQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
		.setImage(m_image);

	if (newLayout == vk::ImageLayout::eDepthStencilAttachmentOptimal)
	{
		HasStencilComponent(format) ? subresourceRange.setAspectMask(vk::ImageAspectFlagBits::eDepth | vk::ImageAspectFlagBits::eStencil)
			: subresourceRange.setAspectMask(vk::ImageAspectFlagBits::eDepth);
	}
	else
	{
		subresourceRange.setAspectMask(vk::ImageAspectFlagBits::eColor);
	}

	barrier.setSubresourceRange(subresourceRange);


	vk::PipelineStageFlags sourceStage;
	vk::PipelineStageFlags destinationStage;

	if (oldLayout == vk::ImageLayout::eUndefined && newLayout == vk::ImageLayout::eTransferDstOptimal)
	{
		barrier.setDstAccessMask(vk::AccessFlagBits::eTransferWrite);

		sourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
		destinationStage = vk::PipelineStageFlagBits::eTransfer;
	}
	else if (oldLayout == vk::ImageLayout::eTransferDstOptimal && newLayout == vk::ImageLayout::eShaderReadOnlyOptimal)
	{
		barrier.setSrcAccessMask(vk::AccessFlagBits::eTransferWrite)
			.setDstAccessMask(vk::AccessFlagBits::eShaderRead);

		sourceStage = vk::PipelineStageFlagBits::eTransfer;
		destinationStage = vk::PipelineStageFlagBits::eFragmentShader;
	}
	else if (oldLayout == vk::ImageLayout::eUndefined && newLayout == vk::ImageLayout::eDepthStencilAttachmentOptimal)
	{
		barrier.setDstAccessMask(vk::AccessFlagBits::eDepthStencilAttachmentRead | vk::AccessFlagBits::eDepthStencilAttachmentWrite);

		sourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
		destinationStage = vk::PipelineStageFlagBits::eEarlyFragmentTests;

	}
	else if (oldLayout == vk::ImageLayout::eUndefined && newLayout == vk::ImageLayout::eColorAttachmentOptimal)
	{
		barrier.setDstAccessMask(vk::AccessFlagBits::eColorAttachmentRead | vk::AccessFlagBits::eColorAttachmentWrite);

		sourceStage = vk::PipelineStageFlagBits::eTopOfPipe;
		destinationStage = vk::PipelineStageFlagBits::eColorAttachmentOutput;
	}
	else
	{
		throw std::invalid_argument("unsupported layout transition");
	}

	//{} no bit set for dependency flags (third param), can't use 0 with .hpp bindings
	commandBuffer.pipelineBarrier(sourceStage, destinationStage, {}, nullptr, nullptr, barrier);
}

//don't forget to call BeginSingleTimeCommands before this and EndSingleTimeCommands after this
void Texture::GenerateMipmaps(vk::Format imageFormat, vk::CommandBuffer commandBuffer, int32_t texWidth, int32_t texHeight, uint32_t mipLevels)
{
	auto formatProperties = m_physicalDevice.getFormatProperties(imageFormat);

	if (!(formatProperties.optimalTilingFeatures & vk::FormatFeatureFlagBits::eSampledImageFilterLinear))
	{
		throw std::runtime_error("Texture image format doesn't support linear blitting");
	}

	auto barrier = vk::ImageMemoryBarrier()
		.setImage(m_image)
		.setSrcQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED)
		.setDstQueueFamilyIndex(VK_QUEUE_FAMILY_IGNORED);
	barrier.subresourceRange.setAspectMask(vk::ImageAspectFlagBits::eColor)
		.setBaseArrayLayer(0)
		.setLayerCount(1)
		.setLevelCount(1);

	auto mipWidth = texWidth;
	auto mipHeight = texHeight;

	for (uint32_t i = 1; i < mipLevels; ++i)
	{
		barrier.setOldLayout(vk::ImageLayout::eTransferDstOptimal)
			.setNewLayout(vk::ImageLayout::eTransferSrcOptimal)
			.setSrcAccessMask(vk::AccessFlagBits::eTransferWrite)
			.setDstAccessMask(vk::AccessFlagBits::eTransferRead)
			.subresourceRange.setBaseMipLevel(i - 1);

		commandBuffer.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eTransfer, {}, nullptr, nullptr, barrier);

		auto blit = vk::ImageBlit();
		blit.srcOffsets[0] = { 0,0,0 };
		blit.srcOffsets[1] = { mipWidth, mipHeight, 1 };
		blit.srcSubresource.setAspectMask(vk::ImageAspectFlagBits::eColor)
			.setMipLevel(i - 1)
			.setBaseArrayLayer(0)
			.setLayerCount(1);
		blit.dstOffsets[0] = { 0,0,0 };
		blit.dstOffsets[1] = { mipWidth > 1 ? mipWidth / 2 : 1, mipHeight > 1 ? mipHeight / 2 : 1, 1 };
		blit.dstSubresource.setAspectMask(vk::ImageAspectFlagBits::eColor)
			.setMipLevel(i)
			.setBaseArrayLayer(0)
			.setLayerCount(1);

		commandBuffer.blitImage(m_image, vk::ImageLayout::eTransferSrcOptimal, m_image, vk::ImageLayout::eTransferDstOptimal, blit, vk::Filter::eLinear);

		barrier.setOldLayout(vk::ImageLayout::eTransferSrcOptimal)
			.setNewLayout(vk::ImageLayout::eShaderReadOnlyOptimal)
			.setSrcAccessMask(vk::AccessFlagBits::eTransferRead)
			.setDstAccessMask(vk::AccessFlagBits::eShaderRead);

		commandBuffer.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader, {}, nullptr, nullptr, barrier);

		if (mipWidth > 1)
		{
			mipWidth /= 2;
		}
		if (mipHeight > 1)
		{
			mipHeight /= 2;
		}
	}

	barrier.subresourceRange.setBaseMipLevel(mipLevels - 1);
	barrier.setOldLayout(vk::ImageLayout::eTransferDstOptimal)
		.setNewLayout(vk::ImageLayout::eShaderReadOnlyOptimal)
		.setSrcAccessMask(vk::AccessFlagBits::eTransferWrite)
		.setDstAccessMask(vk::AccessFlagBits::eShaderRead);

	commandBuffer.pipelineBarrier(vk::PipelineStageFlagBits::eTransfer, vk::PipelineStageFlagBits::eFragmentShader, {}, nullptr, nullptr, barrier);
}

void Texture::CreateImageView(vk::Format format, vk::ImageAspectFlags aspectFlags, uint32_t mipLevels)
{
	auto subResourceRange = vk::ImageSubresourceRange()
		.setAspectMask(aspectFlags)
		.setBaseMipLevel(0)
		.setLevelCount(mipLevels)
		.setBaseArrayLayer(0)
		.setLayerCount(1);

	auto createInfo = vk::ImageViewCreateInfo()
		.setImage(m_image)
		.setViewType(vk::ImageViewType::e2D)
		.setFormat(format)
		//.setComponents(mapping)
		.setSubresourceRange(subResourceRange);

	m_imageView =  m_logicalDevice.createImageView(createInfo);
}
//////////////////////////////////////////////////////////////////////////
//---DUPLICATED FROM MAIN - put into a memory class?
//////////////////////////////////////////////////////////////////////////
uint32_t Texture::FindMemoryType(uint32_t typeFilter, vk::MemoryPropertyFlags properties)
{
	auto memProperties = m_physicalDevice.getMemoryProperties();

	for (uint32_t i = 0; i < memProperties.memoryTypeCount; ++i)
	{
		if ((typeFilter & (1 << i)) && (memProperties.memoryTypes[i].propertyFlags & properties) == properties)
		{
			return i; //return the index of the memory type that satisfies everything we are looking for
		}
	}

	throw std::runtime_error("failed to find suitable memory type");
}

bool Texture::HasStencilComponent(vk::Format format)
{
	return format == vk::Format::eD32SfloatS8Uint || format == vk::Format::eD24UnormS8Uint;
}

